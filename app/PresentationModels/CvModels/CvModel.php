<?php
namespace App\PresentationModels\CvModels;

class CvModel{
	public $id;
	public $name;
	public $gender;
	public $email;
	public $phone;
	public $jobInterestedArea;
	public $file;
	public $date;
}
?>