<?php

namespace App\Http\Controllers;

use App\Models\Cv;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{

    public function store($cvId, Request $request)
    {

        $cv           = Cv::find($cvId);
        $cv->hasPayed = true;
        $cv->save();
      
        $this->sendEmailToUser($cv);
        $this->sendEmailToAdmin($cv);

        $notification = array(
            'message'    => 'You have paid successfully.You cv has been added.',
            'alert-type' => 'success',
        );

        return redirect('/cv/create')->withNotification($notification);
    }

    private function sendEmailToUser($cv)
    {

        \Mail::send('emails.sendToUser',
            [

                'message' => "You have Successfully paid and added your cv",

            ],
            function ($message) use ($cv) {

                $message->from(env('MAIL_TO_ADDRESS'));
                $message->to($cv->email);

            });


    }
    private function sendEmailToAdmin($cv)
    {
        $admin = User::find(1);

        \Mail::send('emails.sendToAdmin',
            [

                'email' => $cv->email,
                'link'  => 'cvsite.test/cv/' . $cv->id,

            ],
            function ($message) use ($cv, $admin) {

                $message->from(env('MAIL_TO_ADDRESS'));
                $message->to($admin->email);

            });
        return redirect('/login');

    }
    private function sendEmail($cv, $password)
    {

        \Mail::send('emails.credentials',
            [

                'email'    => $user->email,
                'password' => $password,

            ],
            function ($message) use ($user) {

                $message->from(env('MAIL_FROM_ADDRESS'));
                $message->to($user->email);

            });

    }
}
