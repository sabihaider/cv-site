<?php

namespace App\Http\Controllers;

use App\Models\Cv;
use App\Models\Setting;
use App\Services\Interfaces\ICvService;
use Illuminate\Http\Request;

class CvController extends Controller
{

    public $CvService;

    public function __construct(ICvService $cvService)
    {
        $this->CvService = $cvService;
        $this->middleware('auth',['except' => ['logout','create','store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cvs = $this->CvService->getAllCvs();
        return view('cv/index')->withCvs($cvs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cv/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cv = $this->CvService->create($request);


        $cvId=$cv->id;

        $settings=Setting::find(1);
        $amount=$settings->amount;

        return view('payment')->withCvId($cvId)->withAmount($amount);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cv $cv)
    {
        $cv = $this->CvService->getCv($cv);
        return view("cv/show")->withCv($cv);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cv $cv)
    {
        return view('cv.edit')->withCv($cv);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cv $cv)
    {
        $this->CvService->update($request, $cv);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cv $cv)
    {
        $this->cvService->delete($cv);
    }
}
