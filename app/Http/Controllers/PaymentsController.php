<?php

namespace App\Http\Controllers;

use Braintree_Transaction;
use Illuminate\Http\Request;
use Paypalpayment;
use App\Models\Setting;

class PaymentsController extends Controller
{
    
    public function process(Request $request)
    {
        $settings=Setting::find(1);
        $payload = $request->input('payload', false);
        $nonce   = $payload['nonce'];

        $status = Braintree_Transaction::sale([
            'amount'             => $settings->amount,
            'paymentMethodNonce' => $nonce,
            'options'            => [
                'submitForSettlement' => true,
            ],
        ]);

        return response()->json($status);
    }


     public function paywithPaypal($cvId)
    {
        // ### Address
        // Base Address object used as shipping or billing
        // address in a payment. [Optional]
        $shippingAddress= Paypalpayment::shippingAddress();
        $shippingAddress->setLine1("3909 Witmer Road")
            ->setLine2("Niagara Falls")
            ->setCity("Niagara Falls")
            ->setState("NY")
            ->setPostalCode("14305")
            ->setCountryCode("US")
            ->setPhone("716-298-1822")
            ->setRecipientName("Jhone");

        // ### Payer
        // A resource representing a Payer that funds a payment
        // Use the List of `FundingInstrument` and the Payment Method
        // as 'credit_card'
        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("paypal");
        
        $item = Paypalpayment::item();
        $item->setName('Cv')
                ->setDescription('Pay For Cv');

        //$item2 = Paypalpayment::item();

        // $item2->setName('Granola bars')
        //         ->setDescription('Granola Bars with Peanuts')
        //         ->setCurrency('USD')
        //         ->setQuantity(5)
        //         ->setTax(0.2)
        //         ->setPrice(2);


        // $itemList = Paypalpayment::itemList();
        // $itemList->setItems([$item])
        //     ->setShippingAddress($shippingAddress);


        // $details = Paypalpayment::details();
        // $details->setShipping("1.2")
        //         ->setTax("1.3")
        //         //total of items prices
        //         ->setSubtotal("17.5");

        //Payment Amount
        $settings=Setting::find(1);        
        $amount = Paypalpayment::amount();
        $amount->setCurrency("USD")
                // the total is $17.8 = (16 + 0.6) * 1 ( of quantity) + 1.2 ( of Shipping).
                ->setTotal($settings->amount);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. Transaction is created with
        // a `Payee` and `Amount` types

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent as 'sale'
        $redirectUrls = Paypalpayment::redirectUrls();
        $redirectUrls->setReturnUrl(url("/payment/success/".$cvId))
            ->setCancelUrl(url("/payment/fails"));

        $payment = Paypalpayment::payment();

        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        try {
            // ### Create Payment
            // Create a payment by posting to the APIService
            // using a valid ApiContext
            // The return object contains the status;
            $payment->create(Paypalpayment::apiContext());
        } catch (\PPConnectionException $ex) {
            return response()->json(["error" => $ex->getMessage()], 400);
        }

        return redirect($payment->getApprovalLink());
    }
}
