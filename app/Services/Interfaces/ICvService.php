<?php 
namespace App\Services\Interfaces;

interface ICvService
{

	public function create($request);
	public function update($request,$cv);
	public function getCv($cv);
	public function delete($cv);
}

?>