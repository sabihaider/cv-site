<?php
namespace App\Services;

use Illuminate\Http\Request;
use Validator;
use App\Services\Interfaces\IValidationService;

class ValidationService implements IValidationService
{
	
	public function validateCv($request){

		$validate = Validator::make($request->all(),array(
			'firstName' => 'required|max:255',
			'lastName' => 'required|max:255',
			'email'	=>'required|email|unique:cvs,email',
			'phone' => 'nullable|numeric||max:99999999999999999999',
			'jobInterestedArea' => 'nullable'
		))->validate();
	}
}

?>