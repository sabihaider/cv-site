<?php

namespace App\Services;

use App\Models\Setting;
use App\Services\Interfaces\ISettingService;

class SettingService implements ISettingService
{
	public function updateSetting($request,$setting)
	{
		$this->setupSetting($request,$setting);

	}

	public function setupSetting($request,$setting)
	{
		$setting->paypal_client_id = $request->paypalClientId;
		$setting->paypal_client_secret = $request->paypalClientSecret;
		$setting->amount = $request->amount;
		$setting->save();
	}
}

?>