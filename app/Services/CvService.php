<?php 
namespace App\Services;

use App\Models\Cv;
use Illuminate\Http\File;
use App\Services\Interfaces\ICvService;
use App\PresentationModels\CvModels\CvModel;

class CvService implements ICvService{


	public function getAllCvs()
	{
		$cvs = Cv::orderBy('updated_at', 'desc')->get();
		$cvs = $this->getCvs($cvs);
		return $cvs;
	}

	public function create($request)
	{
		$cv = new Cv;
		$this->setupCv($request,$cv);

		return $cv;
	}

	public function update($request,$cv)
	{
		return $this->setupCv($request,$cv);
	}

	public function getCvs($cvs)
	{
		$cvModelArray = [];
		foreach ($cvs as $cv) {
			$cvModel =  $this->getcv($cv);
			array_push($cvModelArray, $cvModel);
		}
		return $cvModelArray;
	}

	public function getCv($cv)
	{
		$cvModel = new CvModel;
		$cvModel->id = $cv->id;
		$cvModel->name = $cv->firstName . ' ' . $cv->lastName;
		$cvModel->gender = $cv->gender;
		$cvModel->email = $cv->email;
		$cvModel->phone = $cv->phone;
		$cvModel->jobInterestedArea = $cv->jobInterestedArea;
		$cvModel->file = $cv->file;
		$cvModel->date = $cv->updated_at;
		return $cvModel;
	}

	public function delete($cv)
	{
		$cv->delete();
	}

	private function setupCv($request,$cv)
	{
		$cv->firstName = $request->firstName;
		$cv->lastName = $request->lastName;
		$cv->gender = $request->gender;
		$cv->email = $request->email;
		$cv->phone = $request->phone;
		$cv->jobInterestedArea = $request->jobInterestedArea;
		if ($request->hasFile('file')) {
			$file = $request->file('file');
			$fileName = $file->getClientOriginalName();
			$location = public_path('cvs/' . $fileName);
            $file->move($location, $fileName);
            $cv->file = $fileName;
		}
		$cv->save();
	}
}
?>