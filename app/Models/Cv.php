<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cv extends Model
{
    //

    public function user() {
        return $this->hasOne('cvsite\Models\User');
    }
}
