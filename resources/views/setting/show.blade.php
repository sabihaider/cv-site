@extends('layouts.app')
@section('content')
    <article class="main-heading">
        <div class="container">
            <div class="row-content100">
                <div class="col-xs-12">
                    <h1 class="text-center">Settings</h1>
                </div>
            </div>
        </div>
    </article>
    <section class="cv-view">
        <div class="container">
            <div class="row row-content">
                <div class="col-xs-12">
                    <div class="panel-body">
                        <div class='col-md-6 col-md-offset-3'>
                            <ul class="list-group">
                                <li class="list-group-item"><strong>Paypal Client Id:</strong>{{$setting->paypal_client_id}}</li>
                                <li class="list-group-item"><strong>Paypal Client Secret:</strong> {{ $setting->paypal_client_secret }}</li>
                                <li class="list-group-item"><strong>Amount:</strong>    {{$setting->amount}}</li>
                            </ul>
                            <a href="{{ route('settings.edit',$setting->id) }}" class="btn btn-default">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
