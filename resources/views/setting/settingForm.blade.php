<div class="row ">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group ">
            <label for="paypalClientId">Paypal Client Id</label>
            <input type="text" name="paypalClientId" id="paypalClientId" class="form-control"
                   @if(isset($setting->paypal_client_id)) value="{{$setting->paypal_client_id}}" @else placeholder="Paypal Client Id" @endif required >
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group ">
            <label for="paypalClientSecret">Paypal Client Secret</label>
            <input type="text" name="paypalClientSecret" id="paypalClientSecret" class="form-control"
                   @if(isset($setting->paypal_client_secret)) value="{{$setting->paypal_client_secret}}" @else placeholder="Paypal Client Secret" @endif required >
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group ">
            <label for="amount">Amount</label>
            <input type="text" name="amount" id="amount" class="form-control"
                   @if(isset($setting->amount)) value="{{$setting->amount}}" @else placeholder="Amount" @endif required >
        </div>
    </div>
</div>
<div class="row">
    <div class="row-content100">
        <div class="col-xs-12 col-sm-4 col-sm-offset-4">
            <div class="form-group">
                <button type="submit" class="button40 button">
                    <i class=""></i> {{isset($setting)? "Update": "Add"}} CV
                </button>
            </div>
        </div>
    </div>
</div>
