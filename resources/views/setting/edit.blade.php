@extends('layouts.app')
@section('content')
    <section class="cv-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row row-content">
                        <div class="col-md-9 col-md-offset-3">
                            <div class='panel-body'>
                                <!-- Display Validation Errors -->
                                {{-- @include('common.errors') --}}
                                <form action="{{ route('settings.update',$setting->id) }}" method="POST"
                                      class="form-horizontal">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    @include('setting.settingForm')
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
