@extends('layouts.app')
@section('content')
 
         
            @if (Route::has('login'))
               {{--  <div class="top-right links"> --}}
                    @auth
                        {{-- <a href="{{ url('/home') }}">Home</a> --}}
                    @else
                       {{--  <a href="{{ route('login') }}">Login</a> --}}
                       {{--  <a href="{{ route('register') }}">Register</a> --}}
                    @endauth
               {{--  </div> --}}
            @endif
<header class="jumbotron jumbotron-main ">

       <!-- Main component for a primary marketing message or call to action -->

       <div class="container" >
           <div class="row row-header">
               <div class="col-xs-12">
                  <div class="flex-hero">
                   <h1 class="text-center">Future in Dubai </h1>
                   <p class="text-center">
                     orem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor. Donec hendrerit volutpat viverra. 
                   </p>
                   </div>
              </div>
          </div>
       </div>
  </header>
   
 <section class="second-section">
    <div class="container">
    <div class="row">
    <div class="col-xs-12">
   <div class="row" style="background: #f5f8fa;">
   <article class="email-article">
     <div class="flex-form">
       <form class="form-inline">
    <div class="form-group ">
     <label for="exampleInputName2">Recieve News Alerts & Updates:</label>
   <input type="text" class="form-control" id="exampleInputName2" placeholder="Enter your email here">
    </div>
    <div class="form-group fc">
    <button type="submit" class="btn btn-info">Send</button>
    </div>
    </form>
    </div>
  </article>
      <article class="recruit-article">
       <div class="col-xs-12 col-lg-3">
         <h1>A.</h1>
         <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor. Donec hendrerit volutpat viverra. Nunc metus ipsum, dignissim vel accumsan vel, convallis id risus. Vestibulum ultricies fringilla dui sed pretium. Sed sed ligula feugiat, fermentum sem ut, sagittis est. Vivamus hendrerit aliquam arcu, id placerat est luctus eget. Suspendisse potenti. Donec sed nibh tellus. Maecenas congue scelerisque dignissim.</p>
       </div>
       <div class="col-xs-12 col-lg-3">
         <h1>B.</h1>
         <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor. Donec hendrerit volutpat viverra. Nunc metus ipsum, dignissim vel accumsan vel, convallis id risus. Vestibulum ultricies fringilla dui sed pretium. Sed sed ligula feugiat, fermentum sem ut, sagittis est. Vivamus hendrerit aliquam arcu, id placerat est luctus eget. Suspendisse potenti. Donec sed nibh tellus. Maecenas congue scelerisque dignissim.</p>
       </div>
       <div class="col-xs-12 col-lg-3">
         <h1 >C.</h1>
         <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor. Donec hendrerit volutpat viverra. Nunc metus ipsum, dignissim vel accumsan vel, convallis id risus. Vestibulum ultricies fringilla dui sed pretium. Sed sed ligula feugiat, fermentum sem ut, sagittis est. Vivamus hendrerit aliquam arcu, id placerat est luctus eget. Suspendisse potenti. Donec sed nibh tellus. Maecenas congue scelerisque dignissim.</p>
       </div>
       <div class="col-xs-12 col-lg-3">
         <h1>D.</h1>
         <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h5>
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor. Donec hendrerit volutpat viverra. Nunc metus ipsum, dignissim vel accumsan vel, convallis id risus. Vestibulum ultricies fringilla dui sed pretium. Sed sed ligula feugiat, fermentum sem ut, sagittis est. Vivamus hendrerit aliquam arcu, id placerat est luctus eget. Suspendisse potenti. Donec sed nibh tellus. Maecenas congue scelerisque dignissim.</p>
       </div>
       </article>
     </div>
      <div class="row">
             <article class="carousel-1">
             <h2 class="text-center">Lorem ipsum dolor sit amet</h2>
               <div class="container" style="width:90%">
                  <div class="classe owl-carousel owl-theme">
                    <div class="item">
                    <div class="text-center">
                      <i class="fa fa-address-card  fa-3x"></i>
                    </div>
                    <h3 class="text-center">1. Licence</h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor</p>
                      
                    </div>
                    <div class="item">
                     <div class="text-center">
                   <i class="fa fa-address-card  fa-3x"></i>
                    </div>
                    <h3 class="text-center">2. Licence</h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor</p>
                    </div>
                    <div class="item">
                     <div class="text-center">
                      <i class="fa fa-user-circle fa-3x"></i>
                    </div>
                    <h3 class="text-center">3. Licence</h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor</p>
                    </div>
                    <div class="item">
                      <div class="text-center">
                      <i class="fa fa-address-card  fa-3x"></i>
                    </div>
                    <h3 class="text-center">4. Licence</h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor</p>
                    </div>
                    <div class="item">
                      <div class="text-center">
                      <i class="fa fa-address-card  fa-3x"></i>
                    </div>
                    <h3 class="text-center">5. Licence</h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor</p>
                    </div>
                    <div class="item">
                     <div class="text-center">
                      <i class="fa fa-address-card  fa-3x"></i>
                    </div>
                    <h3 class="text-center">6. Licence</h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor</p>
                    </div>
                    <div class="item">
                     <div class="text-center">
                      <i class="fa fa-address-card  fa-3x"></i>
                    </div>
                    <h3 class="text-center">7. Licence</h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor</p>
                    </div>
                </div>
          </div>
          </article>
          </div>
       </div>
    </div>
  </div>
</section>
   <section class="third">
    <div class="container">
    <div class="row">
    <div class="col-xs-12">
        <div class="call-to-act ">
          <h1>Business Management</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros
           ut porttitor</p>
           <a href="#" class="btn button button40">More info</a>
        </div>
    </div>
    </div>
    </div>
</section>
<section class="quote-section">
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-lg-12 col-sm-12">
    <div class="testimonial-quote group">
    <div class="row">
          <div class="col-xs-12 col-xs-offset-0 col-lg-3 col-lg-offset-1 ifc">
             <img src="http://placehold.it/120x120"  class="img-responsive">
          </div>
         <div class="col-xs-12 col-xs-pull-0 col-xs-offset-0 col-lg-6 col-lg-pull-1 col-lg-offset-0 col-sm-offset-2 col-sm-10">
           <div class="quote-container">
              <blockquote>
                  <p>Overall, fantastic! I'd recommend them to anyone looking for a creative, thoughtful, and professional team.”</p>
              </blockquote>  
              <cite><span>Kristi Bruno</span><br>
                  Social Media Specialist<br>
                 (Job Seeker)
              </cite>
          </div>
         </div>
    </div>
    <div class="row">
         <div class="col-xs-12 col-lg-12">
            <div class="quote-flex">
           <h2 >
             Have Questions? <br> Ask a specialist 
           </h2>
           <h3 ><i class="fa fa-mobile fa-2x"></i></h3>
           <h1>800 123 1234 1234</h1>
           <h5 >7 days a week <br> From 8:00am <br>to 2:00pm</h5>
           <i class="fa fa-comments btn-online "> ONLINE SUPPORT CHAT</i>
        </div>
         </div>
    </div>
    </div>    
  </div>
</div>
</section> 
@endsection
