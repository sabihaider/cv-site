@extends('layouts.app')
@section('content')
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12" >
					<div class="col-md-6 col-md-offset-4">
						<h1>Admin Dashboard</h1>
						<div>
						  <a href="{{ route('cv.index') }}">Cv List</a>
						  <a href="{{ route('settings.show',1) }}">Settings</a>
						  <a href="#">Banner</a>
						  <a href="#">Promotional Banners</a>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection