@extends('layouts.app')
@section('content')
   @if (count($cvs) > 0)
    <article class="cv-list-section">
       
        <div class="container">
            <div class="row">
                <div class="col-md-12 content-center">
                    <h3>List of Cvs</h3>
                    <div class="table-responsive">
                        <table class="table table-condensed  table-bordered table-striped table-hover">
                            <thead>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Job Interested Area</th>
                            <th>Actions</th>
                            <th>Posted On</th>
                            </thead>
                            <tbody>
                            @foreach ($cvs as $cv)
                                <tr>
                                    <td>{{ $cv->name }}</td>
                                    <td>{{ $cv->gender }}</td>
                                    <td>{{ $cv->email }}</td>
                                    <td>{{ $cv->phone }}</td>
                                    <td>{{ $cv->jobInterestedArea }}</td>
                                    <td>{{  date('M j, Y h:ia', strtotime($cv->date)) }}</td>
                                    <td>
                                        <a href="{{ route('cv.show',$cv->id) }}" class="btn btn-default button20 btn-sm">Show</a> 
                                        <a href=""  class="btn btn-primary button20 btn-sm">Download</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                      @else
                          <h3>Currently, there are no Cv's</h3>
                    </div>
                </div>
            </div>
        </div>
    </article>
@endif
@endsection
