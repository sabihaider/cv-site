@extends('layouts.app')
@section('content')
    <article class="main-heading">
        <div class="container">
            <div class="row-content100">
                <div class="col-xs-12">
                    <h1 class="text-center">CV Details</h1>
                </div>
            </div>
        </div>
    </article>
    <section class="cv-view">
        <div class="container">
            <div class="row row-content">
                <div class="col-xs-12">
                    <div class="panel-body">
                        <div class='col-md-6 col-md-offset-3'>
                            <ul class="list-group">
                                <li class="list-group-item"><strong>ID:</strong>  {{$cv->id}}</li>
                                <li class="list-group-item"><strong>Name:</strong>   {{$cv->name}}</li>
                                <li class="list-group-item"><strong>Gender:</strong> {{ $cv->gender }}</li>
                                <li class="list-group-item"><strong>Email:</strong>    {{$cv->email}}</li>
                                <li class="list-group-item"><strong>Phone:</strong>    {{ $cv->phone  }}</li>
                                <li class="list-group-item"><strong>Job Interested Area:</strong>   {{ $cv->jobInterestedArea  }}</li>
                                <li class="list-group-item"><strong>Posted On:</strong>{{  date('M j, Y h:ia', strtotime($cv->date)) }}</li>
                            </ul>
                            <a href="" class="btn btn-primary">Download</a><p>{{ $cv->file }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
