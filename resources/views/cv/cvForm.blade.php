<div class="row ">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group ">
            <label for="firstName">First Name <span class="error">*</span></label>
            <input type="text" name="firstName" id="firstName" class="form-control"
                   @if(isset($cv->firstName)) value="{{$cv->firstName}}" @else placeholder="First Name" @endif required >
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group ">
            <label for="lastName">Last Name <span class="error">*</span></label>
            <input type="text" name="lastName" id="lastName" class="form-control"
                   @if(isset($cv->lastName)) value="{{$cv->lastName}}" @else placeholder="Last Name" @endif required >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group">
            <label for="gender">Gender</label><br>
            <input name="gender" type="radio" value="male" 
            @if(isset($cv->gender) && $cv->gender == 'male') 
                checked="checked" 
            @endif ><label for="male"> Male</label><br>
            <input name="gender" type="radio" value="female" 
            @if(isset($cv->gender) && $cv->gender == 'female')) 
                checked="checked"
            @endif><label for="female"> Female</label>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group ">
            <label for="email">Email <span class="error">*</span></label>
            <input type="email" name="email" id="email" class="form-control"
                   @if(isset($cv->email)) value="{{$cv->email}}" @else placeholder="Email" @endif required >
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group ">
            <label for="phone">Phone <span class="error">*</span></label>
            <input type="number" name="phone" id="phone" class="form-control"
                   @if(isset($cv->phone)) value="{{$cv->phone}}" @else placeholder="Phone" @endif required >
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group ">
            <label for="jobInterestedArea">Job Interested Area</label>
            <input type="text" name="jobInterestedArea" id="jobInterestedArea" class="form-control"
                   @if(isset($cv->jobInterestedArea)) value="{{$cv->jobInterestedArea}}" @else placeholder="Job Interested Area" @endif required >
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-sm-7 ">
        <div class="form-group ">
            <label for="file">Upload A CV</label>
            <input type="file" name="file" id="file">
            @if(isset($cv->file)) 
                {{$cv->file}}
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="row-content100">
        <div class="col-xs-12 col-sm-4 col-sm-offset-4">
            <div class="form-group">
                <button type="submit" class="button40 button">
                    <i class=""></i> {{isset($cv)? "Update": "Add"}} CV
                </button>
            </div>
        </div>
    </div>
</div>
