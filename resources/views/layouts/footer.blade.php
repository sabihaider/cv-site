<footer class="row-footer">
       <div class="container">
           <div class="row">            
               <div class="col-xs-12  col-lg-3">
                   <h5>Navigation</h5>
                   <hr>
                   <ul class="list-unstyled">
                       <li><a href="#">Home</a></li>
                       <li><a href="#">WHO WE ARE</a></li>
                       <li><a href="#">WHAT WE DO</a></li>
                       <li><a href="#">SERVICES</a></li>
                       <li><a href="#">NEWS</a></li>
                       <li><a href="#">CONTACTS</a></li>
                   </ul>
               </div>
               <div class="col-xs-12  col-lg-3">
               <h5>Our Perception</h5>
               <hr>
               <p>
                 orem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra pulvinar eros ut porttitor. Donec hendrerit volutpat viverra. Nunc metus ipsum, dignissim vel accumsan vel, convallis id risus
               </p>
               </div>
               <div class="col-xs-12 col-lg-3">
                <h5>Facebook lite box</h5>
                <hr>
                  <div class="panel panel-default">
                    <div class="panel-heading fc">
                      <p class="panel-title">Find us on Facebook </p>
                      <span class="col-md-offset-1 col-sm-offset-1 col-xs-offset-1">facebook</span>
                    </div>
                    <div class="panel-body">
                      <div class="media">
                        <div class="media-left media-middle">
                          <a href="#">
                           <img src="{{ asset('images/welcome/media.jpg') }}" class="media-object"
                                  alt="img">
                            
                          </a>
                        </div>
                         <h4 >Media heading</h4>
                          
                    </div>
                    </div>
                  </div>
               </div>
               <div class="col-xs-12 col-lg-3">
                   <h5>Quick Contact</h5>
                    <hr>
                   <address>
                      121, Clear Water Bay Road<br>
                      Clear Water Bay, Kowloon<br>
                      Dubai<br>
                      <i class="fa fa-phone"></i>: +852 1234 5678<br>
                      <i class="fa fa-fax"></i>: +852 8765 4321<br>
                      <i class="fa fa-envelope"></i>:
                       <a href="mailto:dubai@cv.net">dubai@cv.net</a>
                   </address>
                    <ul class="list-inline">
                       <li>
                       <li>
                       <a class="btn btn-social-icon btn-facebook" href="http://www.facebook.com/profile.php?id="><i class="fa fa-facebook  "></i></a>  
                       </li>
                         <a class="btn btn-social-icon btn-google-plus" href="http://google.com/+"><i class="fa fa-google-plus"></i></a>
                       </li>
                       <li>
                         <a class="btn btn-social-icon btn-twitter" href="http://twitter.com/"><i class="fa fa-twitter"></i></a>
                      </li>
                       <li>
                          <a class="btn btn-social-icon btn-linkedin" href="http://www.linkedin.com/in/"><i class="fa fa-linkedin"></i></a>
                       </li>
                    </div>
              
               
               <div class="col-xs-12">
                   <p style="padding:10px;"></p>
                   <p align=center>© Copyright 2018</p>
               </div>
           </div>
       </div>
   </footer>