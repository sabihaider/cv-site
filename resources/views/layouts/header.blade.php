<header class="head-top-area">
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-lg-offset-0 col-sm-6 col-sm-offset-3">
            <img src="{{ asset('images/logo.png') }}"
               class="img-responsive"  alt="MEC-Logo" width="295px" height="90px">
        </div>
        <div class="col-lg-3 col-lg-offset-2 col-xs-offset-0 col-xs-12">
            <div class="header-flex">
                    <i class="fa fa-phone fa-4x"></i>
                    <div>
                    <p> +92-300123456 
                    <br> WhatsApp Online Support</p> 
                    </div>
            </div>
        </div>
        <div class="col-lg-4 fct">
        <div class="pull-right">
        <i class="fa fa-user-circle fa-2x"></i>
        <span>Mustafa LoggedIn</span></br>
        <input type="button" name="" class="button button40" style="margin-top: 0;" value="Register Now">
        </div>
        </div>
    </div>
</div>
     
</header>
 