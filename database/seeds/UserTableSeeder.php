<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
          'id' => '1',
          'name' => 'admin',
          'email' => 'admin@cvsite.com',
          'password' => Hash::make('P@ssword')
        ];
        User::create($user);
    }
}
