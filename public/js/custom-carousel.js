$(".classe").owlCarousel({
      autoPlay: 5000, //Set AutoPlay to 3 seconds
      items : 3,
      navigation : true,
      pagination: false,
      slideSpeed : 500,
      paginationSpeed : 2000,
       responsive: true,
      singleItem:false,
      navigationText: [
      "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>"
      ],
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
      itemsMobile : [479,1]
  });