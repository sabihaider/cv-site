<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/cv','CvController');
Route::resource('/settings','SettingController');
Route::get('/dashboard','DashboardController@index')->name('dashboard.index');


Route::get('/payment/with-paypal/{cvid}','PaymentsController@paywithPaypal');
Route::get('/payment/success/{cvid}/','RegisterController@store');

Route::get('/payment/fails/err',function(){
	return "fails";
});

Route::get('/payment/process', 'PaymentsController@process')->name('payment.process');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
